package com.chhuang.tutool.ui.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.view.KeyEvent
import android.view.View
import androidx.core.view.ViewCompat
import androidx.core.view.ViewPropertyAnimatorListener
import com.chhuang.tutool.R
import com.chhuang.tutool.databinding.ActivityMainBinding
import com.chhuang.tutool.databinding.ActivitySplashBinding
import com.gnetek.tool.base.BaseActivity
import org.jetbrains.anko.find

@SuppressLint("CustomSplashScreen")
class SplashActivity: BaseActivity() {

    private val viewBinding: ActivitySplashBinding by lazy { ActivitySplashBinding.inflate(layoutInflater) }

    override fun viewBinding() {
        setContentView(viewBinding.root)
    }

    override fun initData() {
        super.initData()
        //动画缩放x轴方向与y轴方向，时间为2秒，监听动画结束后跳转到主界面
        ViewCompat.animate(find(R.id.splashImageView)).scaleX(1f).scaleY(1f)
            .setListener(object: ViewPropertyAnimatorListener{
                override fun onAnimationStart(view: View) {}

                override fun onAnimationEnd(view: View) {
                    //动画结束之后进入主界面
                    startActivityAndFinish<MainActivity>()
                }

                override fun onAnimationCancel(view: View) {}
            }).duration = 2000
    }

    /**
     * 按两下返回键的退出
     */
    private var exitTime:Long = 0
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_DOWN) {
            if (System.currentTimeMillis() - exitTime > 2000) {
                toasts(R.string.press_back_again_finish)
                exitTime = System.currentTimeMillis()
            } else {
                finish()
            }
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
}
package com.chhuang.tutool.ui.activity

import android.os.Build
import android.view.View
import com.chhuang.tutool.R
import com.chhuang.tutool.databinding.ActivityConnectionBinding
import com.chhuang.tutool.modle.SocketBean
import com.chhuang.tutool.ui.fragment.TCPClientFragment
import com.chhuang.tutool.ui.fragment.TCPServerFragment
import com.chhuang.tutool.ui.fragment.UDPFragment
import com.gnetek.tool.base.BaseActivity

class ConnectionActivity : BaseActivity() {

    private val viewBinding: ActivityConnectionBinding by lazy { ActivityConnectionBinding.inflate(layoutInflater) }

    private var socketConnectBean: SocketBean? = null

    override fun viewBinding() {
        setContentView(viewBinding.root)
    }

    override fun initView() {
        socketConnectBean = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.getParcelableExtra("socketBean", SocketBean::class.java)
        }else{
            intent.getParcelableExtra("socketBean")
        }
        when(socketConnectBean?.socketType){
            0->{
                socketConnectBean?.let {
                    addFragment(TCPServerFragment(it.port), R.id.fragmentFrameLayout)
                }
            }
            1->{
                socketConnectBean?.let {
                    addFragment(TCPClientFragment(it.ip, it.port), R.id.fragmentFrameLayout)
                }
            }
            2->{
                socketConnectBean?.let {
                    addFragment(UDPFragment(it.ip, it.port, it.loaclPort), R.id.fragmentFrameLayout)
                }
            }
        }
    }

    override fun initListener() {
        //点击 返回
        viewBinding.titleBar.setLeftClickListener {
            finish()
        }
    }
}
package com.chhuang.tutool.ui.fragment

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chhuang.tutool.R
import com.chhuang.tutool.adapter.MessageListAdapter
import com.chhuang.tutool.databinding.FragmentTcpClientBinding
import com.chhuang.tutool.modle.MessageBean
import com.gnetek.tool.base.BaseFragment
import com.gnetek.tool.socket.NettyClient
import com.swallowsonny.convertextlibrary.hex2ByteArray
import com.swallowsonny.convertextlibrary.toHexString
import com.xuexiang.xui.utils.WidgetUtils
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.nio.charset.Charset

class TCPClientFragment(private val ip:String, private val port:Int): BaseFragment() {
    companion object {
        private const val TAG = "TCPClientFragment"
    }

    private lateinit var binding: FragmentTcpClientBinding

    private var tcpClient: NettyClient? = null

    //列表
//    private var messageBean = mutableListOf<MessageBean>()
    //列表适配器
    private val messageListAdapter = MessageListAdapter(mutableListOf())

    override fun bindView(inflater: LayoutInflater,
                          container: ViewGroup?): View {
        binding = FragmentTcpClientBinding.inflate(inflater, container, false)
        val view: View = binding.root
        return view
    }

    override fun initView() {
        WidgetUtils.initRecyclerView(binding.messageRecyclerView)
        binding.messageRecyclerView.adapter = messageListAdapter
    }

    override fun initListener() {
        binding.sendButton.setOnClickListener {
            val content = binding.contentEditText.text
            if(content.isNotBlank()) {
                val message = content.toString()
                Log.e(TAG, "initListener: ${message}")
                val data = if(binding.hexSwitchButton.isChecked){
                    message.hex2ByteArray()
                }else {
//                    message.ascii2ByteArray()
                    message.toByteArray(Charsets.UTF_8)
//                    message.toByteArray(Charset.forName("GBK"))
                }
                tcpClient?.let {
                    it.sendMessage(data)
                    val local = it.getLocalSocketAddress()
                    val messageBean = if(local!=null){
                        MessageBean(if(local.address.hostAddress==null)"local sender" else local.address.hostAddress,
                            local.port,
                            message)
                    }else {
                        MessageBean("local sender", -1, message)
                    }
                    messageListAdapter.loadOne(messageBean)
                    xToasts(R.string.send_success, 1)
                    binding.contentEditText.text = null
                }
            }
        }
    }

    override fun initData() {
        doAsync {
            NettyClient(ip, port, {
                tcpClient = it
                uiThread {
                    xToasts(R.string.connect_success, 1)
                }
            },{//连接失败
                uiThread {
                    xToasts(R.string.connect_failed,0)
                }
            },{
                uiThread {
                    xToasts(R.string.connect_disconnect,0)
                    requireActivity().finish()
                }
            },{
                val datas = it
                uiThread {
                    val message = if(binding.hexSwitchButton.isChecked){
                        datas.toHexString()
                    }else {
//                        datas.readStringBE(0, datas.size,"ascii")
                        String(datas, Charsets.UTF_8)
//                        String(datas, Charset.forName("GBK"))
                    }
                    Log.e(TAG, "initData: ${message}" )
                    val messageBean = MessageBean(ip, port, message,false)
                    messageListAdapter.loadOne(messageBean)
                }
            })
        }
    }

    override fun onDestroy() {
        tcpClient?.destroy()
        super.onDestroy()
    }
}
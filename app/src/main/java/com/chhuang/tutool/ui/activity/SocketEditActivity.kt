package com.chhuang.tutool.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.chhuang.tutool.R
import com.chhuang.tutool.databinding.ActivitySocketEditBinding
import com.chhuang.tutool.db.DatabaseHelper
import com.chhuang.tutool.modle.SocketBean
import com.gnetek.tool.base.BaseActivity
import com.xuexiang.xui.widget.picker.widget.builder.OptionsPickerBuilder
import com.xuexiang.xui.widget.picker.widget.listener.OnOptionsSelectListener
import org.jetbrains.anko.error

class SocketEditActivity : BaseActivity() {
    companion object{
        private const val TAG = "SocketEditActivity"
    }

    private var socketEditBean: SocketBean? = null

    private val viewBinding:ActivitySocketEditBinding by lazy { ActivitySocketEditBinding.inflate(layoutInflater) }

    private lateinit var socketTypeParams: Array<String>
    private var selectedOption = 0

    //数据库
    private val Context.database: DatabaseHelper get() = DatabaseHelper(this)

    override fun viewBinding() {
        setContentView(viewBinding.root)
    }

    override fun initView() {
        socketEditBean = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.getParcelableExtra("socketBean", SocketBean::class.java)
        }else{
            intent.getParcelableExtra("socketBean")
        }
        socketTypeParams = this.resources.getStringArray(R.array.socket_type)
        socketEditBean?.let {
            error { "${it.socketType}  ${it.ip}:${it.port}" }
            selectedOption = it.socketType
            viewBinding.ipEditText.setText(it.ip)
            viewBinding.portEditText.setText(it.port.toString())
            viewBinding.localPortEditText.setText(it.loaclPort.toString())
        }

        viewBinding.socketTypeEditText.setText(socketTypeParams[selectedOption])

        showEditView()
    }

    /**
     * 显示输入框
     */
    private fun showEditView(){
        when(selectedOption){
            0->{
                viewBinding.ipLinearLayout.visibility = View.GONE
                viewBinding.portLinearLayout.visibility = View.VISIBLE
                viewBinding.localPortLinearLayout.visibility = View.GONE
            }
            1->{
                viewBinding.ipLinearLayout.visibility = View.VISIBLE
                viewBinding.portLinearLayout.visibility = View.VISIBLE
                viewBinding.localPortLinearLayout.visibility = View.GONE
            }
            2->{
                viewBinding.ipLinearLayout.visibility = View.VISIBLE
                viewBinding.portLinearLayout.visibility = View.VISIBLE
                viewBinding.localPortLinearLayout.visibility = View.VISIBLE
            }
        }
    }

    override fun initListener() {
        //点击 返回
        viewBinding.titleBar.setLeftClickListener {
            finish()
        }
        //点击 连接类型选择
        viewBinding.socketTypeEditText.setOnClickListener {
            //关闭软键盘
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            val v = window.peekDecorView()
            if (null != v) {
                imm.hideSoftInputFromWindow(v.windowToken, 0)
            }
            //弹出菜单
            val optionPicker = OptionsPickerBuilder(this, object : OnOptionsSelectListener{
                override fun onOptionsSelect(
                    view: View?,
                    options1: Int,
                    options2: Int,
                    options3: Int
                ): Boolean {
                    viewBinding.socketTypeEditText.setText(socketTypeParams[options1])
                    selectedOption = options1
                    showEditView()
                    return false
                }
            }).setTitleText(getString(R.string.picker_socket_type_title))
                .setSelectOptions(selectedOption)
                .build<Any>()

            optionPicker.setPicker(socketTypeParams)
            optionPicker.show()
        }
        //点击 保存
        viewBinding.saveButton.setOnClickListener {
            var bool = false
            val socketBean = SocketBean()
            when(selectedOption){
                0->{
                    when(true){
                        viewBinding.portEditText.text!!.isBlank() -> xToasts(R.string.invalid_port_error, 0)
                        else->{
                            socketBean.socketType = selectedOption
                            socketBean.port = viewBinding.portEditText.text.toString().toInt()
                            bool = true
                        }
                    }
                }
                1->{
                    when(true){
                        viewBinding.ipEditText.text!!.isBlank() -> xToasts(R.string.invalid_ip_error, 0)
                        viewBinding.portEditText.text!!.isBlank() -> xToasts(R.string.invalid_port_error, 0)
                        else->{
                            socketBean.socketType = selectedOption
                            socketBean.ip = viewBinding.ipEditText.text.toString()
                            socketBean.port = viewBinding.portEditText.text.toString().toInt()
                            bool = true
                        }
                    }
                }
                2->{
                    when(true){
                        viewBinding.ipEditText.text!!.isBlank() -> xToasts(R.string.invalid_ip_error, 0)
                        viewBinding.portEditText.text!!.isBlank() -> xToasts(R.string.invalid_port_error, 0)
                        viewBinding.localPortEditText.text!!.isBlank() -> xToasts(R.string.invalid_port_error, 0)
                        else->{
                            socketBean.socketType = selectedOption
                            socketBean.ip = viewBinding.ipEditText.text.toString()
                            socketBean.port = viewBinding.portEditText.text.toString().toInt()
                            socketBean.loaclPort = viewBinding.localPortEditText.text.toString().toInt()
                            bool = true
                        }
                    }
                }
            }

            if (bool) {
                if(socketEditBean!=null){
                    socketBean.id = socketEditBean!!.id
                    database.update(socketBean)
                }else {
                    database.insert(socketBean)
                }
                xToasts(R.string.save_success, 1)

                val intent = Intent()
                intent.putExtra("isChanged", true)
                setResult(RESULT_OK, intent)
                finish()
            }
        }
    }

    override fun initData() {

    }
}
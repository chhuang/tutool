package com.chhuang.tutool.adapter

import android.content.Context
import android.view.View
import android.widget.TextView
import com.chhuang.tutool.R
import com.xuexiang.xui.widget.flowlayout.BaseTagAdapter
import org.jetbrains.anko.find

class ChannelFlowAdapter(context: Context): BaseTagAdapter<String, TextView>(context) {
    override fun newViewHolder(convertView: View?): TextView {
        return convertView!!.find(R.id.tv_tag)
    }

    override fun getLayoutId(): Int {
        return R.layout.adapter_channel_item_layout
    }

    override fun convert(holder: TextView?, item: String?, position: Int) {
        holder?.text=item
    }
}
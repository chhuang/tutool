package com.chhuang.tutool.socket.base

interface ISendable {
    fun parse(): ByteArray
}
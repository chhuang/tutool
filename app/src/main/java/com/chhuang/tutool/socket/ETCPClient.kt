//package com.chhuang.tutool.socket
//
//import android.content.Context
//import android.util.Log
//import com.chhuang.tutool.socket.base.ISendable
//import com.easysocket.EasySocket
//import com.easysocket.config.EasySocketOptions
//import com.easysocket.entity.OriginReadData
//import com.easysocket.entity.SocketAddress
//import com.easysocket.interfaces.conn.SocketActionListener
//
//
//class ETCPClient(context: Context,
//                 ip: String, port: Int,
//                 private val success:(eTCPClient: ETCPClient)->Unit,
//                 private val failed:()->Unit,
//                 private val disconnect:()->Unit,
//                 private val received:(content: ByteArray)->Unit) {
//
//    companion object{
//        private const val TAG = "ETCPClient"
//    }
//
//    init {
//        //全局配置
//        EasySocketOptions.setIsNeedReconnect(true)//是否重连
//        EasySocketOptions.setIsDebug(false)//是否打印调试
//        // socket配置
//        val options = EasySocketOptions.Builder()
//            // 主机地址，请填写自己的IP地址，以getString的方式是为了隐藏作者自己的IP地址
//            .setSocketAddress(SocketAddress(ip, port))
//            // 定义消息协议，方便解决 socket黏包、分包的问题
////            .setReaderProtocol(DefaultMessageProtocol())
//                //设置不使用Delay算法
//            .setTcpNoDelay(true)
//            .build()
//
//        //监听socket相关行为
//        EasySocket.getInstance().createConnection(options, context)
//            .subscribeSocketAction(object : SocketActionListener(){
//                /**
//                 * socket连接成功
//                 * @param socketAddress
//                 */
//                override fun onSocketConnSuccess(socketAddress: SocketAddress?) {
//                    Log.e(TAG, "onSocketConnSuccess: 连接成功 ${socketAddress?.ip} ${socketAddress?.port}" )
//                    success(this@ETCPClient)
//                }
//
//                /**
//                 * socket连接失败
//                 * @param socketAddress
//                 * @param isNeedReconnect 是否需要重连
//                 */
//                override fun onSocketConnFail(socketAddress: SocketAddress?, isNeedReconnect: Boolean) {
//                    Log.e(TAG, "onSocketConnFail: 连接失败，是否需要重连：$isNeedReconnect" )
//                    failed()
//                }
//
//                /**
//                 * socket断开连接
//                 * @param socketAddress
//                 * @param isNeedReconnect 是否需要重连
//                 */
//                override fun onSocketDisconnect(socketAddress: SocketAddress?,isNeedReconnect: Boolean) {
//                    Log.e(TAG, "onSocketDisconnect断开连接，是否需要重连：$isNeedReconnect")
//                    EasySocketOptions.setIsNeedReconnect(false)
//                    disconnect()
//                }
//
//                /**
//                 * socket接收的数据
//                 * @param socketAddress
//                 * @param readData
//                 */
//                override fun onSocketResponse(socketAddress: SocketAddress?, readData: String) {
//    //                Log.e(TAG, "onSocketResponse收到字符串数据-->$readData")
//    //                received(readData.ascii2ByteArray())
//                }
//
//                /**
//                 * socket接收的数据
//                 * @param socketAddress
//                 * @param readData
//                 */
//                override fun onSocketResponse(socketAddress: SocketAddress?, readData: ByteArray?) {
//                    super.onSocketResponse(socketAddress, readData)
//                    readData?.let(received)
//                }
//
//                /**
//                 * socket接收的数据
//                 * @param socketAddress
//                 * @param originReadData
//                 */
//                override fun onSocketResponse(socketAddress: SocketAddress?, originReadData: OriginReadData) {
//    //                Log.e(TAG, "onSocketResponse收到十六进制数据-->" + originReadData.headerData.toHexString())
//    //                Log.e(TAG, "onSocketResponse收到十六进制数据-->" + originReadData.bodyBytes.toHexString())
//    //                Log.e(TAG, "onSocketResponse收到十六进制数据-->" + originReadData.bodyString)
//    //                received(originReadData.bodyBytes)
//                }
//        })
//    }
//
//
//    /**
//     * 发送一个的消息，
//     */
//    fun sendMessage(datas: ByteArray){
//        if(isConnected())
//            EasySocket.getInstance().upMessage(datas)
//    }
//
//    /**
//     * 发送一个的消息，
//     */
//    fun sendMessage(sendable: ISendable){
//        sendMessage(sendable.parse())
//    }
//
//    /**
//     * 已连接
//     */
//    fun isConnected(): Boolean{
//        return EasySocket.getInstance()!=null && EasySocket.getInstance().defconnection!=null
//    }
//
//    /**
//     * 获取本地ip与port信息
//     */
//    fun getLocalSocketAddress(): SocketAddress?{
//        return if(isConnected())
//            EasySocket.getInstance().defconnection.localSocketAddress
//        else
//            null
//    }
//
//    /**
//     * 销毁
//     */
//    fun destroy(){
//        if(isConnected())
//            EasySocket.getInstance().destroyConnection()
//    }
//
//}
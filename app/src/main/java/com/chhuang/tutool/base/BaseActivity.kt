package com.gnetek.tool.base
import android.os.Bundle
import android.view.Gravity
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.xuexiang.xui.utils.XToastUtils
import com.xuexiang.xui.widget.toast.XToast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.runOnUiThread

/**
 * activity的基类
 */
abstract class BaseActivity : AppCompatActivity(), AnkoLogger {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding()
        initView()
        initListener()
        initData()
        info { "onCreate" }
        debug { "onCreate" }
        error { "onCreate" }
        XToast.Config.get() //位置设置为居中
            .setGravity(Gravity.CENTER)
    }

    /**
     * 绑定布局
     */
    abstract fun viewBinding()

    /**
     * 初始化view
     */
    protected open fun initView(){

    }

    /**
     * 初始化监听
     */
    protected open fun initListener() {

    }

    /**
     * 初始化数据
     */
    protected open fun initData() {

    }

    /**
     * FragmentManager的扩展函数，它将一个带有接受者的Lambda函数作为传入的参数，而这个FragmentTransaction就是接收者对象
     */
    inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
        beginTransaction().func().commit()
    }

    fun FragmentActivity.addFragment(fragment: Fragment, frameId: Int){
        supportFragmentManager.inTransaction { add(frameId, fragment) }
    }


    fun FragmentActivity.replaceFragment(fragment: Fragment, frameId: Int) {
        supportFragmentManager.inTransaction{replace(frameId, fragment)}
    }

    /**
     * 线程安全的提示信息
     */
    protected fun toasts(message:String){
        runOnUiThread{longToast(message)}
    }

    /**
     * 线程安全的提示信息
     */
    protected fun toasts(messageRes:Int){
        runOnUiThread{longToast(messageRes)}
    }



    /**
     * 线程安全的提示信息
     */
    protected fun xToasts(message: String, position: Int=4){
        try {
            runOnUiThread {
                when (position) {
                    0 -> XToastUtils.error(message)
                    1 -> XToastUtils.success(message)
                    2 -> XToastUtils.info(message)
                    3 -> XToastUtils.warning(message)
                    4 -> XToastUtils.toast(message)
                }
            }
        }catch (e: IllegalStateException){
            e.printStackTrace()
        }
    }

    /**
     * 线程安全的提示信息
     * 0->error 1->success 2->info 3->warning 4->toast
     */
    protected fun xToasts(messageRes: Int, position: Int=4){
        try {
            runOnUiThread {
                when (position) {
                    0 -> XToastUtils.error(messageRes)
                    1 -> XToastUtils.success(messageRes)
                    2 -> XToastUtils.info(messageRes)
                    3 -> XToastUtils.warning(messageRes)
                    4 -> XToastUtils.toast(messageRes)
                }
            }
        }catch (e: IllegalStateException){
            e.printStackTrace()
        }
    }

    /**
     * 开启activity并且关闭当前界面，在打开新的界面中返回不用显示当前界面时用
     */
    inline fun <reified T:BaseActivity> startActivityAndFinish(){
        startActivity<T>()
        finish()
    }

    /**
     * 销毁
     */
    override fun onDestroy() {
        XToast.Config.get() //位置还原
            .resetGravity()
        super.onDestroy()
    }
}
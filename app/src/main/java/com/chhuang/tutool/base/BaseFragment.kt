package com.gnetek.tool.base

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.xuexiang.xui.utils.XToastUtils
import com.xuexiang.xui.widget.toast.XToast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.error
import org.jetbrains.anko.info
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.runOnUiThread

/**
 * Fragment基类
 */
abstract class BaseFragment : Fragment(), AnkoLogger {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
        info { "onCreate" }
        debug { "onCreate" }
        error { "onCreate" }
        XToast.Config.get() //位置设置为居中
            .setGravity(Gravity.CENTER)
    }

    /**
     * Fragment初始化
     */
    open protected fun init() {}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        binding = FragmentViewBindingBinding.inflate(inflater, container, false)
//        val view: View = binding.getRoot()
//        binding.tvContent.setText("这里是在Fragment通过ViewBinding设置的文本")
//        return view

        return bindView(inflater, container)
    }

    /**
     * 获取布局ID
     */
    abstract fun bindView(inflater: LayoutInflater,
                          container: ViewGroup?): View

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initListener()
        initData()
    }


    /**
     * 初始化布局
     */
    protected open fun initView(){

    }

    /**
     * 初始化数据
     */
    protected open fun initData() {
    }

    /**
     * 初始化监听
     */
    protected open fun initListener() {
    }

    /**
     * 线程安全的提示信息
     */
    protected fun toasts(message: String){
        runOnUiThread { longToast(message) }
    }

    /**
     * 线程安全的提示信息
     */
    protected fun toasts(messageRes:Int){
        runOnUiThread{longToast(messageRes)}
    }

    /**
     * 线程安全的提示信息
     * 0->error 1->success 2->info 3->warning 4->toast
     */
    protected fun xToasts(message: String, position: Int=4){
        try {
            runOnUiThread {
                when (position) {
                    0 -> XToastUtils.error(message)
                    1 -> XToastUtils.success(message)
                    2 -> XToastUtils.info(message)
                    3 -> XToastUtils.warning(message)
                    4 -> XToastUtils.toast(message)
                }
            }
        }catch (e: IllegalStateException){
            e.printStackTrace()
        }
    }

    /**
     * 线程安全的提示信息
     * 0->error 1->success 2->info 3->warning 4->toast
     */
    protected fun xToasts(messageRes: Int, position: Int=4){
        try {
            runOnUiThread {
                when (position) {
                    0 -> XToastUtils.error(messageRes)
                    1 -> XToastUtils.success(messageRes)
                    2 -> XToastUtils.info(messageRes)
                    3 -> XToastUtils.warning(messageRes)
                    4 -> XToastUtils.toast(messageRes)
                }
            }
        }catch (e: IllegalStateException){
            e.printStackTrace()
        }
    }

    /**
     * 销毁
     */
    override fun onDestroy() {
        XToast.Config.get() //位置还原
            .resetGravity()
        super.onDestroy()
    }
}
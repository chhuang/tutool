package com.chhuang.tutool.modle

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class SocketBean(var id: Int = -1,
                      var socketType:Int = -1,
                      var ip:String = "",
                      var port:Int = -1,
                      var loaclPort:Int = -1,
                      var modifyTime: Date = Date(),
                      var createTime: Date = Date()) : Parcelable {
    companion object{
        val TABLE_NAME = "Socket"
        val PRIMARY_ID = "_id"
        val SOCKET_TYPE = "socketType"
        val IP = "ip"
        val PORT = "port"
        val LOCAL_PORT = "loaclPort"
        val MODIFY_TIME = "modifyTime"
        val CREATE_TIME = "createTime"
    }
}
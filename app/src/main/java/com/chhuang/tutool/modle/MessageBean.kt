package com.chhuang.tutool.modle

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.Date

@Parcelize
class MessageBean(var ip:String = "",
                  var port:Int = -1,
                  var message:String="",
                  var isLocal:Boolean=true,
                  var time: Date = Date()) : Parcelable {
}